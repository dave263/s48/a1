import React, {Fragment} from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.min.css';

import App from './App';


// ReactDOM.render(what to render, where to render);
  
  ReactDOM.render(
    <Fragment>
  {/*self-closing tag*/}
    <App />
    </Fragment>, document.getElementById('root'));