import {Fragment} from 'react';
import {
  BrowserRouter,
  Routes,
  Route,
} from "react-router-dom";


import Home from './pages/Home'
import Courses from './pages/Courses'
import Register from './pages/Register'
import Login from './pages/Login'
import Error from './pages/Error'

import AppNavbar from './components/AppNavbar'
import Footer from './components/Footer'

function App() {

  return(
    <BrowserRouter>
      <AppNavbar/>
      <Routes>
        <Route path="/" element={ <Home/> } />
        <Route path="/courses" element={ <Courses/> } />
        <Route path="/register" element={ <Register/> } />
        <Route path="/login" element={ <Login/> } />
        <Route path="/:undefined" element={ <Error/> } />
      </Routes>
      <Footer/>
    </BrowserRouter>

  )
}

export default App;
